from rest_framework import routers
from django.urls import path

from apps.order.api.views import create_order

router = routers.DefaultRouter()

urlpatterns = router.urls

urlpatterns += [

    path('create/order/', create_order),
]
