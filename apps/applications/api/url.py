from rest_framework import routers
from django.urls import path

from apps.applications.api.views import *
from apps.applications.api.viewset import *

rputer = routers.DefaultRouter()
rputer.register(r'objects', ObjectsViewSet, basename='object')

urlpatterns = [

    path('objects-list/', ObjectListView.as_view()),
    path('object-detail/<int:pk>/', ObjectDetailView.as_view()),
    path('create-appv2/', CreateContact.as_view()),

]
