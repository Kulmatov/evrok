from rest_framework import generics
from rest_framework.views import APIView
from django.http import Http404
from rest_framework.response import Response
from rest_framework import status
from django.forms.models import model_to_dict
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt

from apps.applications.api.serializers import ObjectsSerializer, ApplicationSerializer
from apps.applications.models import Objects, Applications


class ObjectListView(generics.ListAPIView):
    queryset = Objects.objects.all()
    serializer_class = ObjectsSerializer


class ObjectDetailView(generics.RetrieveAPIView):
    queryset = Objects.objects.all()
    serializer_class = ObjectsSerializer
    lookup_url_kwarg = 'pk'


@api_view(["GET", "POST"])
@csrf_exempt
def create_app(request):
    if request.method == 'GET':

        serializer = ApplicationSerializer()

        return Response(serializer.data)

    elif request.method == 'POST':

        serializer = ApplicationSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CreateContact(generics.CreateAPIView):
    queryset = Applications.objects.all()
    serializer_class = ApplicationSerializer


class ContactCreateView(APIView):
    queryset = Applications.objects.all()

    def get_objects(self, pk):

        try:

            application = Applications.objects.get(id=pk)
            return application

        except Applications.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):

        application = self.get_objects(pk)
        serializer_application = ApplicationSerializer(application)

        return Response(serializer_application.data)

    def post(self, request, format=None):

        if request.method == 'GET':

            serializer_application = ApplicationSerializer()

        else:

            serializer_application = ApplicationSerializer(data=request.data)

            if serializer_application.is_valid():
                name = serializer_application.data.get('name')
                number = serializer_application.data.get('number')
                email = serializer_application.data.get('email')
                sms = serializer_application.data.get('sms')

                app = Applications.objects.create(
                    name=name,
                    number=number,
                    email=email,
                    sms=sms
                )

            return Response({'app': model_to_dict(app)})

    def put(self, request, pk, format=None):

        application = self.get_objects(pk)
        serializer_application = ApplicationSerializer(application, data=request.data)

        if serializer_application.is_valid():
            # serializer_application

            return Response(serializer_application.data)

        return Response(serializer_application.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):

        application = self.get_objects(pk)
        application.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)
