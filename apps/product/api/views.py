from rest_framework import generics
from apps.product.api.serializers import TransportDetailSerializer
from apps.product.models import Transport


class TransportDetailView(generics.RetrieveAPIView):
    queryset = Transport.objects.all()
    serializer_class = TransportDetailSerializer
    lookup_url_kwarg = 'pk'
