from rest_framework import viewsets, mixins
from apps.product.api.filters import *
from apps.product.api.serializers import *
from apps.product.models import Transport


class ProductViewSet(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    queryset = Transport.objects.all()
    serializer_class = TransportSerializer
    filterset_class = TransportFilter


class AllProductViewSet(mixins.ListModelMixin,
                        mixins.RetrieveModelMixin,
                        viewsets.GenericViewSet):
    queryset = Transport.objects.all()
    serializer_class = TransportSerializer


class CategoryViewSet(mixins.ListModelMixin,
                      mixins.RetrieveModelMixin,
                      viewsets.GenericViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class TypeViewSet(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet):
    queryset = Transport.objects.all()
    serializer_class = TransportTypeSerializer
    filterset_class = TypeFilter
